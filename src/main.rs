use std::io::Write;

use actix_multipart::Multipart;
use actix_files::Files;
use actix_web::{web, App, get, post, Error, HttpResponse, HttpServer};
use futures_util::TryStreamExt as _;
use uuid::Uuid;

use askama::Template;

#[derive(Template)]
#[template(path = "index.html", escape = "none")]
struct Index { files: String }

#[post("/")]
async fn upload(mut payload: Multipart) -> Result<HttpResponse, Error> {
    // iterate over multipart stream
    while let Some(mut field) = payload.try_next().await? {
        // A multipart/form-data stream has to contain `content_disposition`
        let content_disposition = field.content_disposition();

        let filename = content_disposition
            .get_filename()
            .map_or_else(|| Uuid::new_v4().to_string(), sanitize_filename::sanitize);
        let filepath = format!("./files/{filename}");

        // File::create is blocking operation, use threadpool
        let mut f = web::block(|| std::fs::File::create(filepath)).await??;

        // Field in turn is stream of *Bytes* object
        while let Some(chunk) = field.try_next().await? {
            // filesystem operations are blocking, we have to use threadpool
            f = web::block(move || f.write_all(&chunk).map(|_| f)).await??;
        }
    }

    Ok(HttpResponse::Ok().into())
}

#[get("/")]
async fn index() -> HttpResponse {
    // Create list of files
    let mut files = String::new();
    for file in std::fs::read_dir("files").unwrap() {
        let file = file.unwrap();
        files += &format!("<li><a href=\"{}\">{}</a></li>", file.path().display(), file.path().display());
    }    

    let html = Index{ files }.render().unwrap(); 

    HttpResponse::Ok().body(html)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "info");
    std::fs::create_dir_all("./files")?;

    HttpServer::new(|| {
        App::new()
            .service(Files::new("/files", "files/").show_files_listing())
            .service(index)
            .service(upload)
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}
